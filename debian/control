Source: classycle
Maintainer: Debian Java Maintainers <pkg-java-maintainers@lists.alioth.debian.org>
Uploaders: Damien Raude-Morvan <drazzib@debian.org>
Section: java
Priority: optional
Build-Depends: ant,
               debhelper-compat (= 13),
               default-jdk
Build-Depends-Indep: ant-optional,
                     maven-repo-helper
Standards-Version: 4.7.0
Vcs-Browser: https://salsa.debian.org/java-team/classycle
Vcs-Git: https://salsa.debian.org/java-team/classycle.git
Homepage: https://classycle.sourceforge.net/

Package: libclassycle-java
Architecture: all
Depends: ant-optional,
         ${misc:Depends}
Description: Analysing tool for Java dependencies
 Classycle's Analyser analyses the static class and package dependencies
 in Java applications or libraries. It is especially helpful for finding cyclic
 dependencies between classes or packages.
 .
 Classycle is similar to JDepend which does also a dependency analysis
 but only on the package level.
 .
 Classycle's Dependency Checker searches for unwanted class dependencies
 described in a dependency definition file. Dependency checking helps to
 monitor whether certain architectural constrains (e.g. in a layered
 architecture) are fulfilled or not.

Package: libclassycle-java-doc
Architecture: all
Section: doc
Depends: ${misc:Depends}
Suggests: libclassycle-java
Description: Analysing tool for Java dependencies - documentation
 Classycle's Analyser analyses the static class and package dependencies
 in Java applications or libraries. It is especially helpful for finding cyclic
 dependencies between classes or packages.
 .
 Classycle is similar to JDepend which does also a dependency analysis
 but only on the package level.
 .
 Classycle's Dependency Checker searches for unwanted class dependencies
 described in a dependency definition file. Dependency checking helps to
 monitor whether certain architectural constrains (e.g. in a layered
 architecture) are fulfilled or not.
 .
 This package contains Javadoc API for libspock-java.
